<?php

class MyClass
{
 public $name = 10;

    /**
     * @param int $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}